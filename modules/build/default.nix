{ lib, ... }:
with lib; {
  options = {
    build = mkOption {
      default = {};
      type = types.submoduleWith {
        modules = [{
          freeformType = with types; lazyAttrsOf (uniq unspecified);
        }];
      };
    };
  };
}
