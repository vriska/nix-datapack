{ pkgs, lib, config, ... }:

with lib;

let
    files' = filter (f: f.enable) (attrValues config.files);

    files = pkgs.runCommand "datapack" {} ''
      set -euo pipefail

      makeFile() {
        src="$1"
        target="$2"

        mkdir -p "$out/$(dirname "$target")"

        if ! [ -e "$out/$target" ]; then
          ln -s "$src" "$out/$target"
        else
          echo "duplicate entry $target -> $src"
          if [ "$(readlink "$out/$target")" != "$src" ]; then
            echo "mismatched duplicate entry $(readlink "$out/$target") <-> $src"
            ret=1

            continue
          fi
        fi
      }

      mkdir -p "$out"

      ${concatMapStringsSep "\n" (fileEntry: escapeShellArgs [
        "makeFile"
        "${fileEntry.source}"
        fileEntry.target
      ]) files'}
    '';

in

{
  options = {
    files = mkOption {
      default = {};
      type = with types; attrsOf (submodule (
        { name, config, options, ... }:
        {
          options = {
            enable = mkOption {
              type = types.bool;
              default = true;
            };

            text = mkOption {
              default = null;
              type = types.nullOr types.lines;
            };

            target = mkOption {
              type = types.str;
            };

            source = mkOption {
              type = types.path;
            };

            json = mkOption {
              default = null;
              type = types.anything;
            };
          };

          config = {
            target = mkDefault name;

            text = mkIf (config.json != null) (mkDerivedConfig options.json builtins.toJSON);
            source = mkIf (config.text != null) (
              let name' = "file-" + baseNameOf name;
              in mkDerivedConfig options.text (pkgs.writeText name')
            );
          };
        }));
    };
  };

  config = {
    build.result = files;
  };
}
