{ lib, config, ... }:

with lib; {
  options = {
    meta = {
      enable = mkOption {
        type = types.bool;
        default = true;
      };

      description = mkOption {
        type = types.nullOr types.jsonText;
        default = null;
      };

      packFormat = mkOption {
        type = types.int;
        default = 10;
      };
    };
  };

  config = lib.mkIf config.meta.enable {
    files."pack.mcmeta".json = {
      pack = {
        description = lib.mkIf (config.meta.description != null) config.meta.description;
        pack_format = config.meta.packFormat;
      };
    };
  };
}
