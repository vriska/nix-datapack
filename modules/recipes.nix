{ lib, config, ... }:

with lib; {
  options = {
    recipes = mkOption {
      default = [];
      type = types.attrsOf (types.submodule [ {
        options =
          let
            common = {
              options = {
                group = mkOption {
                  type = types.nullOr types.str;
                  default = null;
                };
              };
            };

            crafting = {
              imports = [ common ];

              options = {
                result = mkOption {
                  type = types.either types.str (types.submodule [ {
                    options = {
                      item = mkOption {
                        type = types.str;
                      };

                      count = mkOption {
                        type = types.int;
                      };
                    };
                  } ]);
                };
              };
            };

            shaped = {
              imports = [ crafting ];

              # TODO: add more checks
              options = {
                pattern = mkOption {
                  type = types.listOf types.str;
                };

                key = mkOption {
                  type = types.attrsOf (types.either types.str (types.listOf str));
                  default = {};
                };
              };
            };

            shapeless = {
              imports = [ crafting ];

              options = {
                ingredients = mkOption {
                  type = types.listOf (types.either types.str (types.listOf str));
                };
              };
            };

            mkRecipeOption = module: mkOption {
              type = types.attrsOf (types.submodule [ module ]);
              default = {};
            };
          in {
            shaped = mkRecipeOption shaped;
            shapeless = mkRecipeOption shapeless;

            custom = mkOption {
              type = types.attrsOf (types.attrsOf types.unspecified);
              default = {};
            };
          };
      } ]);
    };
  };

  config = {
    files = lib.concatMapAttrs
      (namespace: let
        makeResult = x: if lib.isString x then { item = x; } else x;
        makeIngredient = x:
          let
            chars = lib.stringToCharacters x;
            head = lib.head chars;
            tail = lib.concatStrings (lib.tail chars);
          in
            if head == "#" then
              { tag = tail; }
            else
              { item = x; };
        makeIngredients = x: if lib.isList x then map makeIngredients x else makeIngredient x;

        typeGenerators = {
          shaped = recipe: {
            type = "minecraft:crafting_shaped";

            inherit (recipe) pattern;

            key = lib.mapAttrs (k: makeIngredients) recipe.key;

            result = makeResult recipe.result;
          };

          shapeless = recipe: {
            type = "minecraft:crafting_shapeless";

            ingredients = makeIngredients recipe.ingredients;

            result = makeResult recipe.result;
          };
        };

        generateCustomRecipe = type: name: recipe: {
          "${namespace}/recipes/${lib.replaceStrings [":"] ["/"] type}/${name}.json".json = recipe // {
            inherit type;
          };
        };

        generateCustomRecipes = type: recipes: lib.concatMapAttrs (generateCustomRecipe type) recipes;

        generateStandardRecipe = type: name: recipe: {
          "${namespace}/recipes/minecraft/${type}/${name}.json".json = (typeGenerators.${type} recipe) // (if recipe.group == null then {} else { inherit (recipe) group; });
        };

        generateRecipes = type: recipes:
          if type == "custom" then
            lib.concatMapAttrs generateCustomRecipes recipes
          else
            lib.concatMapAttrs (generateStandardRecipe type) recipes;

        generateAllRecipes = lib.concatMapAttrs generateRecipes;
      in
        generateAllRecipes) config.recipes;
  };
}
