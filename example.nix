{ ... }:
{
  meta.description = "An example Nix datapack";

  recipes."example" = {
    shaped."four_chests" = {
      pattern = [ "XXX" "X X" "XXX" ];
      key."X" = "#minecraft:logs";
      result.item = "minecraft:chest";
      result.count = 4;
    };

    shapeless."paper" = {
      group = "shapeless_paper";
      ingredients = [ "minecraft:sugar_cane" "minecraft:sugar_cane" "minecraft:sugar_cane" ];
      result = "minecraft:paper";
    };

    custom."botania:pure_daisy"."stone" = {
      input.type = "block";
      input.block = "minecraft:deepslate";
      output.name = "minecraft:stone";
    };
  };
}
