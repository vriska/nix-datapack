self: super:

rec {
  types = import ./types.nix { lib = self; inherit (super) types; };

  build = import ./build.nix { lib = self; };

  inherit (build) evalDatapack buildDatapack;
}
