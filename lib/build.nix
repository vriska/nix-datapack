{ lib }:
{
  evalDatapack = pkgs: module: lib.evalModules {
    modules = [ {
      _module.args.pkgs = pkgs;
    } module ] ++ (import ../modules);
  };

  buildDatapack = pkgs: module: (lib.evalDatapack pkgs module).config.build.result;
}
