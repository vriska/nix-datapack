{
  outputs = { self, nixpkgs }: {
    lib = nixpkgs.lib.extend (import ./lib);

    packages.x86_64-linux.example = self.lib.buildDatapack nixpkgs.legacyPackages.x86_64-linux ./example.nix;
  };
}
